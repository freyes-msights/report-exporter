package com.msights.report.exporter.services;


import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.framework.ISessionMgr;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.occa.managedreports.IReportAppFactory;
import com.crystaldecisions.sdk.occa.report.application.OpenReportOptions;
import com.crystaldecisions.sdk.occa.report.application.ReportClientDocument;
import com.crystaldecisions.sdk.occa.report.exportoptions.ReportExportFormat;
import com.msights.report.exporter.util.KeyValue;
import com.msights.report.exporter.util.ReportExportUtil;


@Service
public class ReportService
{

   @Value ("${business.objects.username}")
   private String              username;
   @Value ("${business.objects.password}")
   private String              password;
   @Value ("${business.objects.system}")
   private String              system;
   @Value ("${business.objects.authentication}")
   private String              authentication;

   @Autowired
   private ISessionMgr         sessionManager;
   @Autowired
   private ReportExportUtil    reportExportUtil;

   private static final String GET_REPORT_BY_ID_QUERY = "SELECT SI_ID, SI_NAME FROM CI_INFOOBJECTS WHERE SI_ID=";


   public IEnterpriseSession getEnterpriseSession () throws SDKException
   {
      IEnterpriseSession enterpriseSession = null;
      enterpriseSession = sessionManager.logon (username, password, system, authentication);
      return enterpriseSession;
   }


   public KeyValue <String, byte[]> getReportById (final Integer reportId, final ReportExportFormat format)
         throws Exception
   {
      IEnterpriseSession enterpriseSession = null;
      ReportClientDocument reportClientDocument = null;
      final KeyValue <String, byte[]> report = new KeyValue <> ();

      try
      {
         enterpriseSession = getEnterpriseSession ();
         final IInfoStore infoStore = (IInfoStore) enterpriseSession.getService ("InfoStore");
         final IReportAppFactory reportAppFactory = (IReportAppFactory) enterpriseSession
               .getService ("RASReportFactory");

         final IInfoObjects infoObjects = infoStore.query (GET_REPORT_BY_ID_QUERY + reportId);

         if ( !infoObjects.isEmpty ())
         {
            final IInfoObject infoObject = (IInfoObject) infoObjects.get (0);
            reportClientDocument = reportAppFactory.openDocument (infoObject, OpenReportOptions._openAsReadOnly,
                  Locale.US);

            report.setKey (reportClientDocument.displayName ());
            report.setValue (reportExportUtil.export (reportClientDocument, format));
         }
      }

      catch (final Exception ex)
      {
         throw ex;
      }
      finally
      {
         if (null != reportClientDocument)
         {
            reportClientDocument.close ();
            reportClientDocument.dispose ();
         }
         if (null != enterpriseSession)
         {
            enterpriseSession.logoff ();
         }
      }

      return report;
   }
}