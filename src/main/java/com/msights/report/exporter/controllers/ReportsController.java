package com.msights.report.exporter.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crystaldecisions.sdk.occa.report.exportoptions.ReportExportFormat;
import com.msights.report.exporter.services.ReportService;
import com.msights.report.exporter.util.KeyValue;


@RestController
@RequestMapping (value = "/reports")
public class ReportsController
{
   @Autowired
   private ReportService reportService;


   @RequestMapping (value = "/{id}",
         method = RequestMethod.GET)
   public ResponseEntity <byte[]> getReport (@PathVariable ("id") final Integer reportId) throws Exception
   {
      final KeyValue <String, byte[]> result = reportService.getReportById (reportId, ReportExportFormat.PDF);

      final HttpHeaders headers = new HttpHeaders ();
      headers.setContentType (MediaType.parseMediaType ("application/pdf"));

      headers.setContentDispositionFormData (result.getKey (), result.getKey () + ".pdf");
      final ResponseEntity <byte[]> response = new ResponseEntity <byte[]> (result.getValue (), headers, HttpStatus.OK);
      return response;
   }


   @ExceptionHandler (Exception.class)
   public ResponseEntity <String> handleException ()
   {
      final ResponseEntity <String> response = new ResponseEntity <String> ("Something went wrong :(",
            HttpStatus.INTERNAL_SERVER_ERROR);
      return response;
   }
}
