package com.msights.report.exporter.util;


/**
 * Simple Key-Value DTO.
 */
public class KeyValue <K, V>
{
   private K key;
   private V value;


   /**
    * @return the key
    */
   public K getKey ()
   {
      return key;
   }


   /**
    * @param key the key to set
    */
   public void setKey (final K key)
   {
      this.key = key;
   }


   /**
    * @return the value
    */
   public V getValue ()
   {
      return value;
   }


   /**
    * @param value the value to set
    */
   public void setValue (final V value)
   {
      this.value = value;
   }


   public void set (final K key, final V value)
   {
      this.key = key;
      this.value = value;
   }

}
