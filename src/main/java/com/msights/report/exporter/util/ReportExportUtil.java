package com.msights.report.exporter.util;


import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.springframework.stereotype.Component;

import com.crystaldecisions.sdk.occa.report.application.ReportClientDocument;
import com.crystaldecisions.sdk.occa.report.exportoptions.ReportExportFormat;
import com.crystaldecisions.sdk.occa.report.lib.ReportSDKException;


@Component
public class ReportExportUtil
{
   public byte[] export (final ReportClientDocument rcd, final ReportExportFormat format) throws ReportSDKException,
         IOException
   {
      final ByteArrayInputStream stream = (ByteArrayInputStream) rcd.getPrintOutputController ().export (format);
      return read (stream);
   }


   private byte[] read (final ByteArrayInputStream inputStream) throws IOException
   {
      final byte[] array = new byte[inputStream.available ()];
      inputStream.read (array);

      return array;
   }
}