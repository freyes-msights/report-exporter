package com.msights.report.exporter;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.ISessionMgr;


@Configuration
@ComponentScan
@EnableAutoConfiguration
@PropertySource ("classpath:application.properties")
public class Application
{
   public static void main (final String[] args)
   {
      SpringApplication.run (Application.class, args);
   }


   @Bean
   public ISessionMgr getSessionManager () throws SDKException
   {
      return CrystalEnterprise.getSessionMgr ();
   }
}