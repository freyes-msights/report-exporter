#Build and Run.

1. Install [gradle][gradle]
2. run `gradle clean build` in the project directory.
3. run `java -jar report_exporter.jar` in `<project_dir>/build/libs/`
4. Test http://localhost:8080/reports/8421

Browser should offer to save a PDF file as the result of the report execution. Application assumes you have Java 7.

[gradle]: http://www.gradle.org